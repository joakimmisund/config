echo "Setting up the current machine using local"
ANSIBLE_DIR="setup_machine"

sudo apt -y install ansible

ansible-playbook "${ANSIBLE_DIR}/setup.yml" --connection=local -K -i "${ANSIBLE_DIR}/hosts"
bash ../configure.sh
