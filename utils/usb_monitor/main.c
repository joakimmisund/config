#include <fcntl.h>
#include <sys/fanotify.h>
#include <stdlib.h>
#include <stdio.h>
#include <error.h>
#include <unistd.h>
#include <errno.h>
#include <libudev.h>
#include <sys/epoll.h>

#define BUFFER_SIZE sizeof(struct fanotify_event_metadata)*100
#define MAX_EVENTS 10

struct udev_info {
	struct udev *udev;
	struct udev_monitor *monitor;
	int fd;
};

struct udev_info *create_udev_info()
{
	
	struct udev_info *info;

	if (!(info = malloc(sizeof(struct udev_info)))) {
		perror("malloc");
		return NULL;
	}

	if (!(info->udev = udev_new())) {
		perror("udev_new");
		goto error;
	}

	if (!(info->monitor =  udev_monitor_new_from_netlink(info->udev, "udev"))) {
		perror("udev_monitor_new_from_netlink");
		goto error;
	}
	
	if (udev_monitor_filter_add_match_subsystem_devtype(info->monitor, "block", NULL) != 0) {
		perror("udev_monitor_filter_add_match_subsystem_devtype");
		goto error;
	}

	if (udev_monitor_enable_receiving(info->monitor) < 0) {
		perror("udev_monitor_enable_receiving");
		goto error;
	}

	if ((info->fd = udev_monitor_get_fd(info->monitor)) < 0) {
		perror("udev_monitor_get_fd");
		goto error;
	}

	return info;
error:
	free(info);
	return NULL;
}

void destroy_udev_info(struct udev_info *info)
{
	udev_monitor_unref(info->monitor);
	udev_unref(info->udev);

	free(info);
}

int poll_loop()
{

	struct udev_info *info = create_udev_info();


	if (!info) {
		return EXIT_FAILURE;
	}
	
	

	struct epoll_event ev, events[MAX_EVENTS];
	struct udev_device *dev;
	int nfds, epollfd;

	epollfd = epoll_create1(0);
	if (epollfd == -1) {
		perror("epoll_create1");
		exit(EXIT_FAILURE);
	}

	ev.events = EPOLLIN;
	ev.data.fd = info->fd;
	printf("Listening to %d\n", info->fd);
	if (epoll_ctl(epollfd, EPOLL_CTL_ADD, info->fd, &ev) == -1) {
		perror("epoll_ctl: listen_sock");
		exit(EXIT_FAILURE);
	}

	for (;;) {
		nfds = epoll_wait(epollfd, events, MAX_EVENTS, -1);
		if (nfds == -1) {
			perror("epoll_wait");
			exit(EXIT_FAILURE);
		}
		printf("Got activity\n");

		for (int n = 0; n < nfds; ++n) {
			if (events[n].data.fd == info->fd) {
				printf("Activity on %d\n", info->fd);
				dev = udev_monitor_receive_device(info->monitor);
				if (!dev)
					continue;

				printf("action: %s\n", udev_device_get_action(dev));
				printf("devnode: %s\n", udev_device_get_devnode(dev));
				printf("devnum: %lu\n", udev_device_get_devnum(dev));
				printf("devpath: %s\n", udev_device_get_devpath(dev));
				printf("devtype: %s\n", udev_device_get_devtype(dev));
				printf("driver: %s\n", udev_device_get_driver(dev));
				printf("is_initialized: %d\n", udev_device_get_is_initialized(dev));
				printf("subsystem: %s\n", udev_device_get_subsystem(dev));
				printf("sysname: %s\n", udev_device_get_sysname(dev));
				printf("sysnum: %s\n", udev_device_get_sysnum(dev));
				printf("syspath: %s\n", udev_device_get_syspath(dev));




				struct udev_list_entry *list = udev_device_get_devlinks_list_entry(dev);
				do {
					printf("DEV: name: %s, value: %s\n", udev_list_entry_get_name(list), udev_list_entry_get_value(list));
				} while((list = udev_list_entry_get_next(list)));

				list = udev_device_get_properties_list_entry(dev);
				do {
					printf("PROP: name: %s, value: %s\n", udev_list_entry_get_name(list), udev_list_entry_get_value(list));
				} while((list = udev_list_entry_get_next(list)));

				list = udev_device_get_sysattr_list_entry(dev);
				do {
					printf("SYS: name: %s, value: %s\n", udev_list_entry_get_name(list), udev_list_entry_get_value(list));
				} while((list = udev_list_entry_get_next(list)));

				list = udev_device_get_tags_list_entry(dev);
				do {
					printf("TAGS: name: %s, value: %s\n", udev_list_entry_get_name(list), udev_list_entry_get_value(list));
				} while((list = udev_list_entry_get_next(list)));

				list = udev_device_get_current_tags_list_entry(dev);
				do {
					printf("CUR TAGS: name: %s, value: %s\n", udev_list_entry_get_name(list), udev_list_entry_get_value(list));
				} while((list = udev_list_entry_get_next(list)));

				printf("-- \n\n");

				udev_device_unref(dev);
				
			}
		}
	}

}

int main(int argc, char* argv[])
{
	poll_loop();
	

	if (argc < 2) {
		fprintf(stderr, "USAGE: %s <path>\n", argv[0]);
		return EXIT_FAILURE;
	}

	char *path = argv[1];

	int fd = fanotify_init(FAN_CLASS_NOTIF, O_RDONLY);

	if (fd == -1) {
		perror("fanotify_init");
		return EXIT_FAILURE;
	}

	printf("%d %d %d\n", FAN_OPEN, FAN_MODIFY, FAN_ACCESS);

	int ret = fanotify_mark(fd,
				FAN_MARK_ADD | FAN_MARK_MOUNT,
				FAN_OPEN | FAN_MODIFY | FAN_ACCESS,
				0,
				path);

	if (ret != 0) {
		fprintf(stderr, "Error marking %s\n", path);
		perror("fanotify_mark");
		return EXIT_FAILURE;
	}


	char buffer[BUFFER_SIZE];

	while(1) {

		int ret = read(fd, buffer, BUFFER_SIZE);

		if (ret == -1 && errno != EAGAIN) {
			perror("read");
			return EXIT_FAILURE;
		}

		printf("%d\n", ret);

		struct fanotify_event_metadata *cur = (struct fanotify_event_metadata*)buffer;
		for (int offset = 0; offset < ret; offset += cur->event_len) {

			if (FANOTIFY_METADATA_VERSION == cur->vers) {

				if (cur->fd == FAN_NOFD) {
					fprintf(stderr, "Overflow indicated by fd\n");
				} else {
					/* TODO: Use fd to get path, or change the reporting */
				}

				printf("pid: %05d, mask: %02llu, fd: %02d\n", cur->pid, cur->mask, cur->fd);
				
			} else {
				fprintf(stderr, "Received version %u, expected %u\n", cur->vers, FANOTIFY_METADATA_VERSION);
			}
		}
	}
				

	

	
	return EXIT_SUCCESS;
}
