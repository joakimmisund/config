#!/bin/bash

DIR=$(realpath "$(dirname "${BASH_SOURCE[0]}")")

TIMESTAMP="$(date +%Y-%m-%d-%H%M)"
BACKUP_DIR="${DIR}/.backup-${TIMESTAMP}"
mkdir -p $BACKUP_DIR
VERBOSE=1

function verbose_print {
    if [ $VERBOSE ]; then
	echo -e $1
    fi
}

#Copies a file from $1 (replacer) to $2 (target). If $2 exists it is copied to $3 (backup dir)
function link_with_backup {
    FILE_REPLACER=$1
    FILE_TARGET=$2

    BACKUP_FILE="$BACKUP_DIR/${FILE_TARGET##*/}"

    if [[ -f $BACKUP_FILE || -L $BACKUP_FILE ]]; then
	verbose_print "Aborting because $BACKUP_FILE exists. Please delete backup manually"
	return
    fi
    
    if [[ -f $FILE_TARGET || -L $FILE_TARGET ]]; then
	verbose_print "EXECUTING: cp -P $FILE_TARGET $BACKUP_FILE"
	cp -PH $FILE_TARGET $BACKUP_FILE
	verbose_print "EXECUTING: rm $FILE_TARGET"
	rm $FILE_TARGET
    fi

    verbose_print "EXECUTING: ln -s $FILE_REPLACER $FILE_TARGET"
    ln -s $FILE_REPLACER $FILE_TARGET
}

# HOME
link_with_backup "${DIR}/home/.bashrc" "$HOME/.bashrc"
link_with_backup "${DIR}/home/.emacs" "$HOME/.emacs"
link_with_backup "${DIR}/home/ssh.config" "$HOME/.ssh/config"
link_with_backup "${DIR}/home/.gitconfig" "$HOME/.gitconfig"
link_with_backup "${DIR}/home/.profile" "$HOME/.profile"
link_with_backup "${DIR}/home/.tmux.conf" "$HOME/.tmux.conf"

# Matplotlib
link_with_backup "${DIR}/matplotlib/matplotlibrc" "$HOME/.config/matplotlib/matplotlibrc"
link_with_backup "${DIR}/matplotlib/stylelib/paper.mplstyle" "$HOME/.config/matplotlib/stylelib/paper.mplstyle"
link_with_backup "${DIR}/matplotlib/stylelib/paper-double-column.mplstyle" "$HOME/.config/matplotlib/stylelib/paper-double-column.mplstyle"



function setup_tmux {
    if [ ! -d "$HOME/.tmux/plugins/tpm" ]; then
	git clone https://github.com/tmux-plugins/tpm $HOME/.tmux/plugins/tpm
    fi
    #Remember to run ctrl-b I (to download the packages)
}
#setup_tmux

function setup_fancontrol {
    sudo apt-get install lm-sensors fancontrol
    sudo sensors-detect
    sudo service kmod start
    sudo pwmconfig
    sudo service fancontrol restart
    sudo service fancontrol start
}

function setup_asus_wifi {
    sudo apt-get install bcmwl-kernel-source
    modprobe wl
}

#setup_fancontrol
#setup_asus_wifi
