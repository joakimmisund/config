;; add mirrors for list-packages
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/") ("melpa" . "http://melpa.org/packages/")))

;; needed to use things downloaded with the package manager
(package-initialize)

;; install some packages if missing
(let* ((packages '(company
		   org-ref
		   magit
		   ido-vertical-mode
		   monokai-theme
		   multiple-cursors
		   iedit
		   undo-tree
		   smex
		   flycheck
		   markdown-mode
		   yaml-mode
		   go-mode
		   rust-mode
		   make-mode
		   ))
       (packages (cl-remove-if 'package-installed-p packages)))
  (when packages
    (package-refresh-contents)
    (mapc 'package-install packages)))

;; python jedi

(add-hook 'python-mode-hook 'jedi:setup)
(add-hook 'python-mode-hook 'jedi:ac-setup)

;; company
(global-company-mode)

;; ido-mode
(ido-mode 1)
(ido-everywhere 1)
(ido-vertical-mode 1)

;; use undo-tree-mode globally
(global-undo-tree-mode)

;; Set mode automatically for the following files
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(add-to-list 'auto-mode-alist '("\\.cu\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cc\\'" . c++-mode))


;; c-programming

(setq c-default-style "linux") ;; Kernel development


;; Emacs behaviour and display

(setq inhibit-splash-screen t)
(setq inhibit-startup-message t)
(show-paren-mode 1)
(column-number-mode 1)
(delete-selection-mode 1)
(blink-cursor-mode 0)
(global-linum-mode)
(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 0)
(load-theme 'monokai t)
(global-set-key (kbd "C-x k") 'kill-this-buffer)
(set-face-attribute 'default nil :height 120 )

(defalias 'yes-or-no-p 'y-or-n-p)
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

(setq-default frame-title-format
   (list '((buffer-file-name " %f"
             (dired-directory
              dired-directory
              (revert-buffer-function " %b"
              ("%b - Dir:  " default-directory)))))))


;; Org mode

(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (C . t)
   (shell . t)
   (latex . t)))
(setq org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
	"pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
	"pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

(setq org-directory "~/.org")
(setq org-default-notes-file (concat org-directory "/notes.org"))
(setq org-todo-keywords '((sequence "TODO" "WAITING" "|" "DONE" "DELEGATED")))
(setq org-log-done 'time)
(setq org-confirm-babel-evaluate nil)

(global-set-key "\C-cs" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-switch)


;; smex

(smex-initialize)
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)

;; eldoc mode

(eldoc-mode 1)

;; flycheck

(global-flycheck-mode)
(add-hook 'text-mode-hook 'turn-on-flyspell)
(add-hook 'prog-mode-hook 'flyspell-prog-mode)


;; Autogen content

(define-skeleton latex-report
  "Inserts template for latex document"
  "\\documentclass[12pt,a4paper,oneside]{article}\n"
  "\\usepackage{fullpage}\n"
  "\\usepackage{parskip}\n"
  "\\usepackage{tikz}\n"
  "\\usepackage{amsmath}\n"
  "\\usepackage{url}\n"
  "\\usepackage{hyperref}\n"
  "\\usepackage[utf8]{inputenc}\n"
  "\\usepackage{cleveref}\n"
  "\\usepackage{listings}\n"
  "\\usepackage{inputenc}\n"
  "\\usepackage{amsthm,amssymb}\n"
  "\\usepackage[toc,page]{appendix}\n"
  "\\usepackage{todonotes}\n"
  "\\usepackage[backend=biber, style=numeric-comp, sorting=ynt]{biblatex}\n"
  "\\usepackage{bbm}\n"
  "\\usepackage{algorithm}\n"
  "\\usepackage{algpseudocode}\n"
  "\\setcounter{tocdepth}{1}\n"
  ""
  "\\title{}\n"
  "\\author{Joakim Misund}\n"
  "\\begin{document}\n"
  "\\maketitle\n"
  "\\tableofcontents\n"
  "\\end{document}\n"
  )
(global-set-key "\C-cr" 'latex-report)

(define-skeleton c-include
  "Insert a precompiler include statement, asking for what to include.
You need to give the quotation marks or the angles yourself."
  "include what? "
  > "#include<" str ">"
  )
(global-set-key "\C-cm" 'c-include)

;; https://github.com/ska2342/ska-init-files/blob/master/dot.emacs.d/init.el

(define-skeleton copyright
  "Inserts template for copyright"
  "Dummy"
  "/*"
  "* Copyright " (format-time-string "%Y") " Joakim Misund <joakim.misund@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */")
(global-set-key "\C-cc" 'copyright)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (company yaml-mode writeroom-mode undo-tree terraform-mode smex rust-mode python-mode org-ref multiple-cursors monokai-theme markdown-mode magit jedi iedit ido-vertical-mode go-mode flycheck ess discover-my-major corfu))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
