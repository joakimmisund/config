# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    #PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    PS1="${debian_chroot:+($debian_chroot)}[\[\e[0;32m\]\u@\h, load: `cat /proc/loadavg | awk '{ print $1; }'`\[\e[00m\]] (\[\e[00;35m\]\d - \t\[\e[00m\])\n\w \$ "
else
    #PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
    PS1="${debian_chroot:+($debian_chroot)}[\u@\h, load: `cat /proc/loadavg | awk '{ print $1; }'`] (\d - \t)\n\w \$ "
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

alias uio='ssh -XY joakimmi@login.ifi.uio.no'
alias ..='cd ..'
alias ddt='ls -trFl'
alias simula='ssh Joakim.Misund@oslo.mlab.no'

os() {
    project=6
    path="$HOME/Uio/INF3151/joakimmi_anderkkv/"
    while [ $project -gt 1 ]; do
	if [ -d $path"P"$project ]; then
	    cd $path"P"$project;
	    break;
	fi

	let project=project-1
    done
}

today() {
    echo -n "Today's date is: "
    date +"%A, %B %-d, %Y"
    echo -e "\n"
    cal
}

howtotarzip() {
    echo "Zipping: tar -zcvf archive.tar.gz directory/ "
    echo "Unzipping: tar -zxvf archive.tar.gz "
}

reset() {
    cd
    clear
}

touchpad_id() {
    xinput list | grep -i touchpad | cut -f2 -d= | cut -d[ -f1
}
touchpad_disable() {
    xinput set-prop $(touchpad_id) "Device Enabled" 0
}
touchpad_enable() {
    xinput set-prop $(touchpad_id) "Device Enabled" 1
}

export PATH=$PATH:/home/joakim/bin
export PATH=$PATH:~/opt/mvn/bin
export PATH=$PATH:/home/joakim/opt/lib
export PATH=$PATH:/usr/local/cuda-8.0/bin
export PATH=$PATH:/home/joakim/Documents/research/scripts/utils
export PATH=$PATH:/home/joakim/Documents/research/scripts/src
export PATH=$PATH:/home/joakim/Documents/research/scripts/setup_env
export PATH="$PATH:/home/joakim/Documents/webrtc_task/depot_tools"
export PATH=$PATH:/home/joakim/.local/bin
alias lss='ls -la | sort -r -g -k 5'
alias gits='git status'
alias mnt_uio='sshfs joakimmi@login.ifi.uio.no:$(uio pwd) /home/joakim/uio/'
alias mnt_uio_storage='sshfs joakimmi@login.uio.no:/net/astra/astra-01/joakimmi /home/joakim/uio_storage/'
alias restartphd='cd Documents/phd/projects/L4S/current_work/'
alias mkdirtoday='mkdir $(date +%Y-%m-%d)'
alias ocarina='ssh -J joakimmi@login.ifi.uio.no ocarina@triforce.ifi.uio.no'
alias cat_trace='sudo cat /sys/kernel/debug/tracing/trace'
alias set_dctcp='sudo sysctl -w net.ipv4.tcp_congestion_control=dctcp'
alias set_dctcp_lo='sudo sysctl -w net.ipv4.tcp_congestion_control=dctcp-lo; echo 1 | sudo tee /sys/module/tcp_dctcp/parameters/paced_chirping_enabled'
alias set_dctcp_lolo='cd /home/joakim/Documents/online_load/tcp_dctcp_loadable && make && sudo make load && sudo sysctl -w net.ipv4.tcp_congestion_control=dctcp-lolo; echo 1 | sudo tee /sys/module/tcp_dctcp_loadable/parameters/paced_chirping_enabled'
alias get_cc='sysctl -a | grep congestion_control'
alias open_pc='emacs ~/Documents/net-next/net/ipv4/paced_chirping.c&'
alias checkcron='grep CRON /var/log/syslog'
alias howtocompile='echo make -j 6 bindeb-pkg LOCALVERSION=-v5.9.0-PRR-fix'

alias gitl='git log --stat --summary'
alias gotoissues='cd Documents/phd/projects/L4S/linux-implementation-issues/alpha_precision_and_update_bug/scripts/'
alias interrupt_stats='S_TIME_FORMAT=ISO mpstat -I SCPU,CPU -u -o JSON'
alias reloadpc='make && sudo make load; sudo sysctl -w net.ipv4.tcp_ecn_fallback=0; sudo sysctl -w net.ipv4.tcp_ecn=1; sudo sysctl -w net.ipv4.tcp_congestion_control=dctcp-pattern'
alias convert_images='for file in $(find /home/joakim/pics/DCIM/100APPLE -name "*.HEIC"); do name=$(basename $file); heif-convert $file ~/pics-converted/${name/%.HEIC/.jpg}; echo $name; done'
alias how_to_install_perf="echo sudo make -C tools/ perf_install prefix=/usr/"
alias how_to_install_perf_remote="echo sudo make LDFLAGS=-static -C tools/ perf; cp tools/perf/perf .."

cc_reload() {
    make
    sudo make load
    sudo sysctl -w net.ipv4.tcp_ecn_fallback=0
    sudo sysctl -w net.ipv4.tcp_ecn=1
    sudo sysctl -w net.ipv4.tcp_congestion_control=$1
}

set_parameter() {
    local module=$1
    local which=$2
    local value=$3

    echo "${value}" | sudo tee /sys/module/${module}/parameters/${which}
}

get_parameter() {
    local module=$1
    local which=$2

    cat /sys/module/${module}/parameters/${which}
}

get_pc_parameters() {
    for name in $(find /sys/module/ -name "paced_chirping*" | grep tcp); do
	echo ${name}:$(cat ${name})
    done
}


if [ -f `which powerline-daemon` ]; then
  powerline-daemon -q
  POWERLINE_BASH_CONTINUATION=1
  POWERLINE_BASH_SELECT=1
  . /usr/share/powerline/bindings/bash/powerline.sh
fi

#if [ -d '/home/joakim/uio/' ]; then
#    mnt_uio &>/dev/null &
#fi

remove_processed_data() {
    dir=$1
    if [ ! -d $dir ]; then
	echo "Could not find $dir"
	return 1
    fi

    for d in $(find $dir -type d -name "processed_data"); do
	rm -v $d/*.csv
    done
}

setup_serial_apu() {
    modprobe usbserial vendor=0x10c4 product=0xea60
    sudo chmod 777 /dev/ttyUSB0
}
